/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "suchai/mainFS.h"
#include "suchai/taskInit.h"
#include "suchai/osThread.h"
#include "suchai/log_utils.h"
#include "app/system/taskHousekeeping.h"
#include "app/system/cmdAPP.h"

static char *tag = "app_main";
static int node = SCH_COMM_NODE;
static int id = 0;
static int duration_s = 0;
static int step_ms = 0;
static int start_s = 0;
static int seconds = 0;

/**
 * App specific initialization routines
 * This function is called by taskInit
 *
 * @param params taskInit params
 */
void initAppHook(void *params)
{
    /** Setup CSP */
    init_setup_libcsp(node); // Re-Init CSP with node address
    /* Set ZMQ interface as a default route*/
    csp_add_zmq_iface(node);

    /** Include app commands */
    cmd_com_init();
    cmd_tm_init();
    cmd_app_init();

    /** Init app tasks */
    int t_ok;
    t_ok = osCreateTask(taskCommunications, "comm", SCH_TASK_COM_STACK, NULL, 2, NULL);
    if(t_ok != 0) LOGE(tag, "Task communications not created!");

    sim_args_t *sim_params = malloc(sizeof(sim_args_t));
    sim_params->sim_start_s = start_s;
    sim_params->sim_end_s = duration_s;
    sim_params->sim_sleep_ms = step_ms / 1;
    sim_params->sim_step_ms = step_ms;
    t_ok = osCreateTask(taskHousekeeping, "housekeeping", 1024, sim_params, 2, NULL);
    if(t_ok != 0) LOGE("simple-app", "Task housekeeping not created!");
}

int main(int argc, char **argv)
{
    /** Parse arguments */
    int opt;
    while((opt = getopt(argc, argv, "n:s:i:")) != -1)
    {
        switch(opt)
        {
            case 'n':
                printf("Node: %c=%s\n", opt, optarg);
                node = atoi(optarg);
                break;
            case 'i':
                printf("Id: %c=%s\n", opt, optarg);
                id = atoi(optarg);
                break;
            case 's':
                printf("Seconds: %c=%s\n", opt, optarg);
                start_s = atoi(optarg);
                break;
            case 'd':
                printf("DeltaT: %c=%s\n", opt, optarg);
                step_ms = atoi(optarg);
                break;
            case 'e':
                printf("End: %c=%s\n", opt, optarg);
                seconds = atoi(optarg);
                break;
            case ':':
                printf("Option needs a value\n");
                break;
            default:
                fprintf(stderr, "Usage: %s -n <node>\n", argv[0]);
                break;
        }
    }

    /** Call framework main, shouldn't return */
    suchai_main();
}
