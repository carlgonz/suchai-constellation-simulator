/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2020, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *      Copyright 2020, Tomas Opazo Toro, tomas.opazo.t@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/taskHousekeeping.h"

#define BACKING_FILE "/suchai_time_ctrl"
#define ACCESS_PERM 0644
#define SEMAPHORE_BASE_NAME "suchai_time_ctrl_sem"

static const char *tag = "Housekeeping";

void taskHousekeeping(void *params)
{
    assert(params != NULL);
    sim_args_t sim_params;
    memcpy(&sim_params, params, sizeof(sim_args_t));
    free(params);

    portTick step_us = sim_params.sim_step_ms*1000;
    long sleep_ns = sim_params.sim_sleep_ms*1000000;
    struct timespec dt = {(time_t)0, sleep_ns};

    int nreaders = 4;
    char sem_names[nreaders][128];
    sem_t* semptrs[nreaders];
    size_t data_size = sizeof(portTick);

    for(int i=0; i<nreaders; i++)
    {
        snprintf(sem_names[i], 128, "%s_%d", SEMAPHORE_BASE_NAME, i);
        printf("sem: %s\n", sem_names[i]);
        sem_unlink(sem_names[i]); /* unlink semaphore files */
    }
    shm_unlink(BACKING_FILE); /* unlink from the backing file */

    int fd = shm_open(BACKING_FILE, O_RDWR | O_CREAT, ACCESS_PERM);
    if (fd < 0) {LOGE(tag, "Can't open shared mem segment..."); osTaskDelete(NULL); return;}

    ftruncate(fd, data_size); /* get the bytes */

    caddr_t memptr = mmap(NULL, data_size,PROT_READ | PROT_WRITE, MAP_SHARED, fd,0);         /* offset: start at 1st byte */
    if ((caddr_t) -1  == memptr) {LOGE(tag, "Can't get segment..."); osTaskDelete(NULL); return;}

    LOGI(tag, "Shared mem address: %p [0..%d]\n", memptr, data_size - 1);
    LOGI(tag, "Backing file:       /dev/shm%s\n", BACKING_FILE );

    /* semahore code to lock the shared mem */
    for(int i=0; i<nreaders; i++)
    {
        sem_t* semptr = sem_open( sem_names[i], O_CREAT, ACCESS_PERM, 0); /* initial value */
        if (semptr == (void*) -1) {LOGE(tag, "Sem_open failed!"); osTaskDelete(NULL); return;}
        semptrs[i] = semptr;
    }

    portTick ticks = 0;

    while(1)
    {
        int op_mode = dat_get_system_var(dat_obc_opmode);
        if( op_mode == DAT_OBC_OPMODE_PAUSE)
        {
            LOGD(tag, "Timer paused!");
            osDelay(1000);
            continue;
        }
        else if(op_mode == DAT_OBC_OPMODE_STOP)
        {
            LOGD(tag, "Timer stopped!");
            break;
        }
        //else DAT_OBC_OPMODE_RUN

        memcpy(memptr, &ticks, data_size); /* copy some bytes to the segment */
        //LOGI(tag, "%u\n", ticks);
        /* increment the semaphore so that all readers can read */
        for(int j=0; j<nreaders; j++)
            sem_post(semptrs[j]);

        nanosleep(&dt, NULL);
        ticks += step_us;
    }

    /* clean up */
    munmap(memptr, data_size); /* unmap the storage */
    close(fd);
    shm_unlink(BACKING_FILE); /* unlink from the backing file */
    for(int i=0; i<nreaders; i++)
    {
        sem_close(semptrs[i]);
        sem_unlink(sem_names[i]);
    }

    osTaskDelete(NULL);
    exit(0);
}
