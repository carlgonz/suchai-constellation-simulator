/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/cmdSIM.h"

static const char* tag = "cmdSIM";
static int sim_running = 0;
static time_t  sim_start_time = 0;
static pthread_cond_t sim_running_cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t sim_running_mutex = PTHREAD_MUTEX_INITIALIZER;

void cmd_sim_init(void)
{
    cmd_add("sim_start", sim_start, "", 0);
    cmd_add("sim_stop", sim_stop, "", 0);
    cmd_add("sim_pause", sim_pause, "", 0);
    cmd_add("sim_set_fp", sim_set_fp, "%d %d %s %n", 4);
    cmd_add("sim_send_fp", sim_send_fp, "%d", 1);
    cmd_add("sim_recv_fp", sim_recv_fp, "%p", 1);
    cmd_add("sim_get_data", sim_take_data, "%d", 1);
    cmd_add("sim_send_data", sim_send_data, "%d %d", 2);
    cmd_add("sim_debug", sim_debug, "", 0);
}

void _sim_wait_state(int state)
{
    pthread_mutex_lock(&sim_running_mutex);
    while(state != sim_running)
        pthread_cond_wait(&sim_running_cond, &sim_running_mutex);
    time_t now = time(NULL);
    time_t dt_s = sim_start_time - now;
    pthread_mutex_unlock(&sim_running_mutex);
    LOGI(tag, "Starting in %d s...", dt_s);
    if(dt_s > 0)
        sleep((unsigned int)dt_s);
}

int _sim_get_state(void)
{
    pthread_mutex_lock(&sim_running_mutex);
    int state = sim_running;
    pthread_mutex_unlock(&sim_running_mutex);
    return state;
}

int sim_start(char *fmt, char *params, int nparams)
{
    dat_set_system_var(dat_obc_opmode, DAT_OBC_OPMODE_RUN);
    return CMD_OK;
}

int sim_pause(char *fmt, char *params, int nparams)
{
    dat_set_system_var(dat_obc_opmode, DAT_OBC_OPMODE_PAUSE);
    return CMD_OK;
}

int sim_stop(char *fmt, char *params, int nparams)
{
    dat_set_system_var(dat_obc_opmode, DAT_OBC_OPMODE_STOP);
    return CMD_OK;
}

int sim_take_data(char* fmt, char* params, int nparams)
{
    int id;
    if(params == NULL || sscanf(params, fmt, &id) != nparams)
        return CMD_ERROR;

    sim_data_t data;
    data.value = id;
    dat_add_payload_sample(&data,sim_sensors);
    osDelay(100); // Simulate the command take some time
    return CMD_OK;
}

int sim_send_data(char* fmt, char* params, int nparams)
{
    int to, id;
    if(params == NULL || sscanf(params, fmt, &to, &id) != nparams)
        return CMD_SYNTAX_ERROR;

    tm_send_tel_from_to(id, id+1, sim_sensors, to);
    return CMD_OK;
}

int sim_set_fp(char* fmt, char* params, int nparams)
{
    int time, node, next;
    char cmd[SCH_CMD_MAX_STR_PARAMS];
    char args[SCH_CMD_MAX_STR_PARAMS];
    memset(cmd, 0, SCH_CMD_MAX_STR_PARAMS);
    memset(args, 0, SCH_CMD_MAX_STR_PARAMS);

    if(params == NULL || sscanf(params, fmt, &time, &node, &cmd, &next) != nparams-1)
        return CMD_SYNTAX_ERROR;

    fp_entry_t fp_entry;
    fp_entry.unixtime = time;
    fp_entry.node = node;
    fp_entry.periodical = 1;
    fp_entry.executions = 1;
    fp_entry.cmd = strdup(cmd);
    fp_entry.args = strdup(params+next);

    int rc = dat_set_fp_st(&fp_entry);
    return rc == 0 ? CMD_OK : CMD_ERROR;
}

int sim_send_fp(char* fmt, char* params, int nparams)
{
    int node;

    if(params == NULL || sscanf(params, fmt, &node) != nparams)
        return CMD_SYNTAX_ERROR;

    //dat_show_fp();

    int fp_entries = dat_get_system_var(dat_fpl_queue);
    if(fp_entries < 1)
        return CMD_ERROR;

    fp_entry_t fp_entry;
    fp_container_sim_t fp_send[fp_entries];
    int index = 0, entry_idx = 0;
    while(entry_idx < fp_entries && dat_pop_fp_st_index(index++, &fp_entry) != -1)
    {
        if(fp_entry.unixtime == ST_FP_NULL)
            continue;

        fp_send[entry_idx].unixtime = csp_hton32(fp_entry.unixtime);
        fp_send[entry_idx].node = csp_hton32(fp_entry.node);
        fp_send[entry_idx].executions = csp_hton32(fp_entry.executions);
        fp_send[entry_idx].periodical = csp_hton32(fp_entry.periodical);
        strncpy(fp_send[entry_idx].cmd, fp_entry.cmd, SIM_FP_LEN);
        strncpy(fp_send[entry_idx].args, fp_entry.args, SIM_FP_LEN);
        entry_idx ++;
    }

    // Send all data over one connection
    int rc = CMD_OK;
    if(fp_entries > 0)
        rc = com_send_telemetry(node, SCH_TRX_PORT_SIM, SCH_TRX_TYPE_SIM_FP, (void *)fp_send, sizeof(fp_send), fp_entries, 0);
    if(rc != CMD_OK)
        return rc;
    return CMD_OK;
}

int sim_recv_fp(char* fmt, char* params, int nparams)
{
    if(params == NULL)
        return CMD_SYNTAX_ERROR;

    fp_container_sim_t *fp_recv = (fp_container_sim_t *)params;
    fp_entry_t fp_entry;
    fp_entry.unixtime = csp_ntoh32(fp_recv->unixtime);
    fp_entry.node = csp_ntoh32(fp_recv->node);
    fp_entry.executions = csp_ntoh32(fp_recv->executions);
    fp_entry.periodical = csp_ntoh32(fp_recv->periodical);
    fp_entry.cmd = strdup(fp_recv->cmd);
    fp_entry.args = strdup(fp_recv->args);
    int rc = dat_set_fp_st(&fp_entry);
    dat_show_fp();
    return rc == SCH_ST_OK ? CMD_OK : CMD_ERROR;
}

int sim_debug(char* fmt, char* params, int nparams)
{
    static unsigned long val = 0;
    time_t t = dat_get_time();
    time_t c = time(NULL);
    val ++;
    LOGR(tag, "clock=%ld, time=%ld, val=%lu", c, t, val);
}