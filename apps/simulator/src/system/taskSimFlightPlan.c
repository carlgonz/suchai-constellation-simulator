/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2020 Matias Ramirez Martinez, nicoram.mt@gmail.com
 *      Copyright 2020, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/taskSimFlightPlan.h"

static const char *tag = "FlightPlan"; 

void taskSimFlightPlan(void *param)
{
    LOGI(tag, "Started");
    uint8_t node = csp_get_address();
    portTick delay_ms = 1000;          //Task period in [ms]
    portTick xLastWakeTime = osTaskGetTickCount();

    time_t elapsed_sec;   // Seconds counter

    while(1)
    {
        osTaskDelayUntil(&xLastWakeTime, delay_ms); //Suspend task
        // Get next command in the flight plan, if any
        elapsed_sec = dat_get_time();
        fp_entry_t fp_entry;
        int rc = dat_get_fp_st((int)elapsed_sec, &fp_entry);
        if(rc == SCH_ST_ERROR)
            continue;

        LOGI(tag, "Command: %s", fp_entry.cmd);
        LOGI(tag, "Arguments: %s", fp_entry.args);
        LOGI(tag, "Node: %d (%d)", fp_entry.node, (int)node);

        if(fp_entry.node != node)
            continue;

        // Update status variables
        dat_set_system_var(dat_fpl_last, (int) elapsed_sec);

        /*If command has to be executed again, set it in flight plan for next execution*/
        if (fp_entry.periodical > 0 && fp_entry.executions > 1)
        {
            fp_entry.executions -= 1;
            fp_entry.unixtime += fp_entry.periodical;
            dat_set_fp_st(&fp_entry);
        }

        /* Save executed commands */
        cmd_data_t cmd_data;
        cmd_data.index = dat_get_system_var(data_map[cmd_sensors].sys_index);
        cmd_data.timestamp = (int)elapsed_sec;
        strncpy(cmd_data.cmd, fp_entry.cmd, SCH_ST_STR_SIZE);
        strncpy(cmd_data.arg, fp_entry.args, SCH_ST_STR_SIZE);
        dat_add_payload_sample(&cmd_data, cmd_sensors);

        /*If command has to be executed*/
        cmd_t *new_cmd = cmd_get_str(fp_entry.cmd);
        cmd_add_params_str(new_cmd, fp_entry.args);
        cmd_send(new_cmd);
    }
}