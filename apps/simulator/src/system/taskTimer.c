/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/taskTimer.h"

#define BACKING_FILE "/suchai_time_ctrl"
#define ACCESS_PERM 0644
#define SEMAPHORE_WRITE_BASE_NAME "suchai_time_ctrl_wr_sem"
#define SEMAPHORE_READ_BASE_NAME "suchai_time_ctrl_rd_sem"

static const char *tag = "Timer";

double _clock(void)
{
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);
    return now.tv_sec + now.tv_nsec*1e-9;
}

void taskTimerPrimary(void *params)
{
    assert(params != NULL);
    sim_args_t sim_params;
    memcpy(&sim_params, params, sizeof(sim_args_t));
    free(params);

    portTick step_us = sim_params.sim_step_ms*1000;
    long sleep_ns = sim_params.sim_sleep_ms*1000000;
    struct timespec dt = {(time_t)0, sleep_ns};

    int nreaders = sim_params.sim_secondaries;
    char sem_wr_names[nreaders][128];
    char sem_rd_names[nreaders][128];
    sem_t* sem_wr_ptrs[nreaders];
    sem_t* sem_rd_ptrs[nreaders];
    size_t data_size = sizeof(portTick);

    LOGI(tag, "Unlink previous backing files...");
    for(int i=0; i<nreaders; i++)
    {
        snprintf(sem_wr_names[i], 128, "%s_%d", SEMAPHORE_WRITE_BASE_NAME, i);
        snprintf(sem_rd_names[i], 128, "%s_%d", SEMAPHORE_READ_BASE_NAME, i);
    }
    shm_unlink(BACKING_FILE); /* unlink from the backing file */

    LOGI(tag, "Create shm backing file: %s", BACKING_FILE)
    int fd = shm_open(BACKING_FILE, O_RDWR | O_CREAT, ACCESS_PERM);
    if (fd < 0) {LOGE(tag, "Can't open shared mem segment..."); osTaskDelete(NULL); return;}

    ftruncate(fd, data_size); /* get the bytes */

    caddr_t memptr = mmap(NULL, data_size,PROT_READ | PROT_WRITE, MAP_SHARED, fd,0);         /* offset: start at 1st byte */
    if ((caddr_t) -1  == memptr) {LOGE(tag, "Can't get segment..."); osTaskDelete(NULL); return;}

    LOGI(tag, "Shared mem backing file:       /dev/shm%s\n", BACKING_FILE );
    LOGI(tag, "Shared mem address: %p [0..%d]\n", memptr, data_size - 1);

    /* semaphore code to lock the shared mem */
    for(int i=0; i<nreaders; i++)
    {
        LOGI(tag, "Creating semaphore %d: %s", i, sem_wr_names[i]);
        sem_t* semptr = sem_open(sem_wr_names[i], O_CREAT, ACCESS_PERM, 0); /* initial value */
        if (semptr == (void*) -1) {LOGE(tag, "Sem_open failed!"); osTaskDelete(NULL); return;}
        sem_wr_ptrs[i] = semptr;

        LOGI(tag, "Creating semaphore rd: %s", sem_rd_names[i]);
        sem_t* sem_rd_ptr = sem_open(sem_rd_names[i], O_CREAT, ACCESS_PERM, 0); /* initial value */
        if (sem_rd_ptr == (void*) -1) {LOGE(tag, "Sem_open failed!"); osTaskDelete(NULL); return;}
        sem_rd_ptrs[i] = sem_rd_ptr;
    }

    portTick sim_ticks = 0;
    portTick real_ticks = 0;
    portTick total_ticks = (sim_params.sim_end_s-sim_params.sim_start_s)*1000000;
    time_t real_time = 0;

    while(sim_ticks < total_ticks)
    {
        int op_mode = dat_get_system_var(dat_obc_opmode);
        if( op_mode == DAT_OBC_OPMODE_PAUSE)
        {
            LOGI(tag, "Timer paused!");
            if(real_ticks > 5*1000000)
            {
                LOGI(tag, "Timer start!");
                dat_set_system_var(dat_obc_opmode, DAT_OBC_OPMODE_RUN);
                //continue;
            }
        }
        else if(op_mode == DAT_OBC_OPMODE_STOP)
        {
            LOGI(tag, "Timer stopped!");
            break;
        }
        else //DAT_OBC_OPMODE_RUN
        {
            /* Sync with readers */
            for(int j=0; j<nreaders; j++) {
                LOGI(tag, "%lu: sem_wait(sem_rd_ptrs[%d])", sim_ticks, j)
                sem_wait(sem_rd_ptrs[j]);
            }

            sim_ticks += step_us;
            memcpy(memptr, &sim_ticks, data_size); /* copy some bytes to the segment */
            //LOGI(tag, "%lu", sim_ticks);

            /* increment the semaphore so that all readers can read */
            for(int j=0; j<nreaders; j++)
                sem_post(sem_wr_ptrs[j]);
        }

        nanosleep(&dt, NULL);
#ifdef SIM
        osTaskSetTickCount(real_ticks += sim_params.sim_sleep_ms*1000);
#endif
        osSetTimeUnix((int64_t)(real_ticks/1000000));
    }

    /* clean up */
    LOGW(tag, "Closing...");
    munmap(memptr, data_size); /* unmap the storage */
    close(fd);
    shm_unlink(BACKING_FILE); /* unlink from the backing file */
    for(int i=0; i<nreaders; i++)
    {
        sem_close(sem_wr_ptrs[i]);
        sem_unlink(sem_wr_names[i]);
        sem_close(sem_rd_ptrs[i]);
        sem_unlink(sem_rd_names[i]);
    }

    cmd_t *cmd = cmd_build_from_str("obc_exit");
    cmd_send(cmd);
}

void taskTimerSecondary(void *params)
{
    // Copy parameters
    assert(params != NULL);
    sim_args_t sim_params;
    memcpy(&sim_params, params, sizeof(sim_args_t));
    free(params);

    char sem_wr_name[128];
    snprintf(sem_wr_name, 128, "%s_%d", SEMAPHORE_WRITE_BASE_NAME, sim_params.sim_id);
    LOGI(tag, "Creating semaphore: %s", sem_wr_name);

    char sem_rd_name[128];
    snprintf(sem_rd_name, 128, "%s_%d", SEMAPHORE_READ_BASE_NAME, sim_params.sim_id);
    LOGI(tag, "Creating semaphore: %s", sem_rd_name);

    int fd = shm_open(BACKING_FILE, O_RDWR, ACCESS_PERM);  /* empty to begin */
    if (fd < 0) perror("Can't get file descriptor...");

    /* get a pointer to memory */
    caddr_t memptr = mmap(NULL,       /* let system pick where to put segment */
                          sizeof(portTick),   /* how many bytes */
                          PROT_READ | PROT_WRITE, /* access protections */
                          MAP_SHARED, /* mapping visible to other processes */
                          fd,         /* file descriptor */
                          0);         /* offset: start at 1st byte */
    if ((caddr_t) -1 == memptr) perror("Can't access segment...");

    /* create a semaphore for mutual exclusion */
    sem_t* sem_wr_ptr = sem_open(sem_wr_name, /* name */
                             O_CREAT,       /* create the semaphore */
                             0644,   /* protection perms */
                             0);            /* initial value */
    if (sem_wr_ptr == (void*) -1) perror("sem_open");

    sem_t* sem_rd_ptr = sem_open(sem_rd_name, /* name */
                                 O_CREAT,       /* create the semaphore */
                                 0644,   /* protection perms */
                                 0);            /* initial value */
    if (sem_wr_ptr == (void*) -1) perror("sem_open");


    portTick ticks = 0;
    //portTick tick_dt = osDefineTime(sim_params.sim_step_ms);
    double seconds_curr = (double)sim_params.sim_start_s;
    //double seconds_dt = sim_params.sim_step_ms / 1000.0;
    double seconds_end = (double)sim_params.sim_end_s;
    // LOGI(tag, "Sec %.4f (%d), Dt: %.4f (%d), End: %.4f (%d)", seconds_curr, sim_params.sim_start_s, seconds_dt, sim_params.sim_step_ms, seconds_end, sim_params.sim_end_s);

#if 0
    char *cmds[4] = {"tle_set 1 42788U 17036Z   20274.00000000  .00002358  00000-0  10003-3 0  9996",
                     "tle_set 2 42788  85.0000  79.0000 0011761  22.3632  67.0000 15.01078627181947",
                     "tle_update",
                     "obc_set_time 1601424625"};

    for(int i=0; i<4; i++)
    {
        cmd_t *cmd = cmd_build_from_str(cmds[i]);
        cmd_send(cmd);
    }
#endif

    double t_ini = 0;
    while(seconds_curr < seconds_end)
    {
#ifdef SIM
        osTaskSetTickCount(ticks);
#endif
        osSetTimeUnix((int64_t)seconds_curr);

        /* use semaphore as a mutex (lock) by waiting for writer to increment it */
        sem_post(sem_rd_ptr);
        LOGD(tag, "%lu: sem_post(sem_rd_ptr[%d])", ticks, sim_params.sim_id);
        if (!sem_wait(sem_wr_ptr)) { /* wait until semaphore != 0 */
            if(ticks == 0)
                t_ini = _clock();
            memcpy(&ticks, memptr, sizeof(ticks));
        }
        else {LOGE(tag, "error %d in sem_wait %d", errno, sim_params.sim_id)};
        seconds_curr = (double)sim_params.sim_start_s + ((double)ticks)*1.0e-6;
        //LOGI(tag, "Ticks: %u (%.6f). Seconds: %.4f (%d), Seconds_end: %.4f", ticks, (double)ticks, seconds_curr, (int64_t)seconds_curr, seconds_end);
    }
    double t_total = _clock() - t_ini;
    double s_total = seconds_curr - (double)sim_params.sim_start_s;
    LOGI(tag, "Execution  time: %.6lf s.", t_total);
    LOGI(tag, "Simulation time: %.4lf s.", s_total);
    LOGI(tag, "Simulation rate: %.4lf X", s_total/t_total);

    /* cleanup */
    LOGW(tag, "Closing...");
    munmap(memptr, 8);
    close(fd);
    sem_close(sem_wr_ptr);
    sem_close(sem_rd_ptr);
    unlink(BACKING_FILE);

    char tm_dump_cmd[SCH_BUFF_MAX_LEN];
    sprintf(tm_dump_cmd, "tm_dump %d tm_status_%d.csv", sta_sensors, sim_params.sim_id);
    cmd_t *cmd = cmd_build_from_str(tm_dump_cmd);
    cmd_send(cmd)

    sprintf(tm_dump_cmd, "tm_dump %d tm_cmds_%d.csv", cmd_sensors, sim_params.sim_id);
    cmd = cmd_build_from_str(tm_dump_cmd);
    cmd_send(cmd)

    cmd = cmd_build_from_str("obc_exit");
    cmd_send(cmd);
}