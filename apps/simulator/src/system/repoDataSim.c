/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/repoDataSim.h"

static const char *tag = "repoDataSim";

int dat_pop_fp_st_index(int index, fp_entry_t *fp_entry)
{
    int rc, node;
    int entries = dat_get_system_var(dat_fpl_queue);
    osSemaphoreTake(&repo_data_sem, portMAX_DELAY);
    //Enter critical zone
    rc = storage_flight_plan_get_idx(index, fp_entry);
    if(rc == SCH_ST_OK && fp_entry->unixtime != ST_FP_NULL)
    {
        rc = storage_flight_plan_delete_row_idx(index);
    }
    //Exit critical zone
    osSemaphoreGiven(&repo_data_sem);

    if(rc == SCH_ST_OK && fp_entry->unixtime != ST_FP_NULL)
    {
        dat_set_system_var(dat_fpl_queue, --entries);
    }
    return rc;
}

int dat_set_fp_st(fp_entry_t *fp_entry)
{
    int entries = dat_get_system_var(dat_fpl_queue);

    osSemaphoreTake(&repo_data_sem, portMAX_DELAY);
    //Enter critical zone
    int rc = storage_flight_plan_set_st(fp_entry);
    //Exit critical zone
    osSemaphoreGiven(&repo_data_sem);
    if(rc == SCH_ST_OK)
        dat_set_system_var(dat_fpl_queue, entries+1);
    else
    LOGE(tag, "Cannot put FP entry (time: %d, entries %d, cmd %s)", fp_entry->unixtime, entries, fp_entry->cmd);
    return rc;
}

int dat_get_fp_st(int elapsed_sec, fp_entry_t* fp_entry)
{
    int rc;
    int entries = dat_get_system_var(dat_fpl_queue);
    osSemaphoreTake(&repo_data_sem, portMAX_DELAY);
    //Enter critical zone
    rc = storage_flight_plan_get(elapsed_sec, fp_entry);
    if(rc == SCH_ST_OK)
        rc = storage_flight_plan_delete_row(elapsed_sec);
    //Exit critical zone
    osSemaphoreGiven(&repo_data_sem);

    if(rc == SCH_ST_OK)
        rc = dat_set_system_var(dat_fpl_queue, --entries);

    return rc;
}