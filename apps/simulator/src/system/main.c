/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "suchai/mainFS.h"
#include "suchai/taskInit.h"
#include "suchai/osThread.h"
#include "suchai/log_utils.h"
#include "suchai/cmdCOM.h"
#include "suchai/cmdTM.h"
#include "suchai/taskCommunications.h"
#include "app/system/cmdAPP.h"
#include "app/system/cmdSIM.h"
#include "app/system/cmdTLE.h"
#include "app/system/taskHousekeeping.h"
#include "app/system/taskTimer.h"
#include "app/system/taskSimFlightPlan.h"

#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>


static char *tag = "app_main";
static int node = SCH_COMM_NODE;
static int id = 0;
static int duration_s = 0;
static int step_ms = 0;
static int start_s = 0;
static int timer_primary = 0;
static int secondaries = 3;

/**
 * App specific initialization routines
 * This function is called by taskInit
 *
 * @param params taskInit params
 */
void initAppHook(void *params)
{
    /** Init timer app */
    int t_ok;
    sim_args_t *sim_params = malloc(sizeof(sim_args_t));
    sim_params->sim_start_s = start_s; //time(NULL);
    sim_params->sim_end_s = sim_params->sim_start_s + duration_s;
    sim_params->sim_step_ms = 200;
    sim_params->sim_sleep_ms = 2;
    sim_params->sim_id = id;
    sim_params->sim_secondaries = secondaries;

    if(timer_primary)
    {
        cmd_sim_init();
        t_ok = osCreateTask(taskTimerPrimary, "sim_clock_p", SCH_TASK_DEF_STACK, sim_params, 1, NULL);
    }
    else
    {
        // Set logging to a file
        // log_set(SCH_LOG_LEVEL, -id);

        /** Setup CSP */
        init_setup_libcsp(node); // Re-Init CSP with node address
        /* Set ZMQ interface as a default route*/
        csp_add_zmq_iface(node);

        /** Include app commands */
        cmd_com_init();
        cmd_tm_init();
        cmd_sim_init();
        cmd_app_init();
        cmd_tle_init();

        /** Init timer task */
        t_ok = osCreateTask(taskTimerSecondary, "sim_clock_s", SCH_TASK_DEF_STACK, sim_params, 1, NULL);
        if(t_ok != 0) LOGE(tag, "Task timer sec. not created!");

        /** Init app tasks */
        t_ok = osCreateTask(taskHousekeeping, "housekeeping", 1024, NULL, 2, NULL);
        if(t_ok != 0) LOGE(tag, "Task housekeeping not created!");

        t_ok = osCreateTask(taskCommunications, "comm", SCH_TASK_COM_STACK, NULL, 2, NULL);
        if(t_ok != 0) LOGE(tag, "Task communications not created!");

        t_ok = osCreateTask(taskSimFlightPlan, "flightplan", SCH_TASK_FPL_STACK, NULL, 2, NULL);
        if(t_ok != 0) LOGE(tag, "Task flight plan not created!");
    }
}

int main(int argc, char **argv)
{
    /** Parse arguments */
    int opt;
    while((opt = getopt(argc, argv, "n:s:i:e:t:l:")) != -1)
    {
        switch(opt)
        {
            case 'n':
                printf("Node: %c=%s\n", opt, optarg);
                node = atoi(optarg);
                break;
            case 'i':
                printf("Id: %c=%s\n", opt, optarg);
                id = atoi(optarg);
                break;
            case 's':
                printf("Start: %c=%s\n", opt, optarg);
                start_s = atoi(optarg);
                break;
            case 'e':
                printf("End: %c=%s\n", opt, optarg);
                duration_s = atoi(optarg);
                break;
            case 't':
                printf("Timer: %c=%s\n", opt, optarg);
                timer_primary = atoi(optarg);
                break;
            case 'l':
                printf("Secondaries: %c=%s\n", opt, optarg);
                secondaries = atoi(optarg);
                break;
            case ':':
                printf("Option needs a value\n");
                break;
            default:
                fprintf(stderr, "Usage: %s -n <node> -i <id> -s <start> -e <end> -t <primary> -l <secondaries>\n", argv[0]);
                exit(1);
        }
    }

    /** Call framework main, shouldn't return */
    suchai_main();
}

#ifdef SIM
void csp_add_zmq_iface(int node)
{
    /* Set ZMQ interface as a default route*/
    uint8_t addr = (uint8_t)node;
    uint8_t *rxfilter = &addr;
    unsigned int rxfilter_count = 1;

    csp_zmqhub_init_w_name_endpoints_rxfilter(CSP_ZMQHUB_IF_NAME,
                                              rxfilter, rxfilter_count,
                                              SCH_COMM_ZMQ_OUT, SCH_COMM_ZMQ_IN,
                                              &csp_if_zmqhub);
    csp_route_set(CSP_DEFAULT_ROUTE, csp_if_zmqhub, CSP_NODE_MAC);
}
#endif