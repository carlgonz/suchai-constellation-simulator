//
// Created by carlos on 08-11-21.
//

#include "suchai/taskCommunications.h"
#include "app/system/cmdSIM.h"

static void sim_receive_tm(csp_packet_t *packet);

void taskCommunicationsHook(csp_conn_t *conn, csp_packet_t *packet)
{
    switch (csp_conn_dport(conn))
    {
        case SCH_TRX_PORT_SIM:
            sim_receive_tm(packet);
            break;
        default:
            break;
    }
}

static void sim_receive_tm(csp_packet_t *packet)
{
    cmd_t *cmd_parse_tm;
    com_frame_t *frame = (com_frame_t *)packet->data;

    frame->nframe = csp_ntoh16(frame->nframe);
    frame->ndata = csp_ntoh32(frame->ndata);

    if(frame->type == SCH_TRX_TYPE_SIM_FP)
    {
        cmd_parse_tm = cmd_get_str("sim_recv_fp");
        cmd_add_params_raw(cmd_parse_tm, frame->data.data8, sizeof(frame->data));
        cmd_send(cmd_parse_tm);
    }
}
