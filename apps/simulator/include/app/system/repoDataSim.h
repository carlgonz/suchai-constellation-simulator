/**
 * @file  repoDataSim.h
 * @date 2021
 * @copyright GNU GPL v3
 *
 * This header is an API to the system's status repository and flight plan repository.
 *
 * The status repository contains the current status of the system.
 *
 * The flight plan repository contains a set of commands the system is set to run.
 *
 * These commands can be timed to execute in the future, execute periodically, and execute a
 * set number of times per periodic cycle.
 */

#ifndef DATA_REPO_SIM_H
#define DATA_REPO_SIM_H

#include "suchai/config.h"
#include "suchai/globals.h"
#include "drivers.h"
#include "suchai/log_utils.h"
#include "suchai/math_utils.h"
#include "suchai/storage.h"
#include "suchai/osSemphr.h"
#include "suchai/osDelay.h"
#include "suchai/repoData.h"
#include "app/system/repoDataSchema.h"

int dat_pop_fp_st_index(int index, fp_entry_t *fp_entry);
int dat_set_fp_st(fp_entry_t *fp_entry);
int dat_get_fp_st(int elapsed_sec, fp_entry_t* fp_entry);


#endif