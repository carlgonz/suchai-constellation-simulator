/**
 * @file  cmdSIM.h
 * @author Carlos Gonzalez C - carlgonz@uchile.cl
 * @author Camilo Rojas C - camrojas@uchile.cl
 * @date 2020
 * @copyright GNU GPL v3
 *
 * This header have definitions of commands related to the flight software
 * simulator
 */

#ifndef _CMDSIM_H
#define _CMDSIM_H

#define SIM_STOP 0
#define SIM_RUN 1

#include "pthread.h"

#include "suchai/repoData.h"
#include "suchai/repoCommand.h"
#include "suchai/cmdTM.h"
#include "app/system/repoDataSim.h"

#define SCH_TRX_PORT_SIM (SCH_TRX_PORT_APP + 0)
#define SCH_TRX_TYPE_SIM_FP 0
#define SIM_FP_LEN 80

typedef struct fp_container_sim{
    int32_t unixtime;               ///< Unix-time, sets when the command should next execute
    int32_t executions;             ///< Amount of times the command will be executed per periodic cycle
    int32_t periodical;             ///< Period of time between executions
    int32_t node;                   ///< Node to execute the command
    char cmd[SIM_FP_LEN]; ///< Command to execute
    char args[SIM_FP_LEN]; ///< Command's arguments
} fp_container_sim_t;

void cmd_sim_init(void);

/**
 * Wait the simulation change to the given status. Blocking.
 * @param state
 */
void _sim_wait_state(int state);
int _sim_get_state(void);

/**
 * Star primary timer, so all secondaries start
 */
int sim_start(char* fmt, char* params, int nparams);

/**
 * Stop primary timer, so all secondaries stop
 */
int sim_stop(char* fmt, char* params, int nparams);

/**
 * Pause primary timer, so all secondaries pause
 */
int sim_pause(char* fmt, char* params, int nparams);

/**
 * Dummy command to simulate taking data
 */
int sim_take_data(char* fmt, char* params, int nparams);

/**
 * Dummy command to simulate sending data
 */
int sim_send_data(char* fmt, char* params, int nparams);

/**
 * Set global flight plan
 * drp_ebf 1010
 * sim_set_fp 1000 1 help
 * sim_send_fp 1
 * @param fmt
 * @param params
 * @param nparams
 * @return
 */
int sim_set_fp(char* fmt, char* params, int nparams);

int sim_send_fp(char* fmt, char* params, int nparams);

int sim_recv_fp(char* fmt, char* params, int nparams);

int sim_debug(char* fmt, char* params, int nparams);


#endif //_CMDSIM_H
