/**
 * @file  taskFlightPlan.h
 * @author Matias Ramirez  - nicoram.mt@gmail.com
 * @author Carlos Gonzalez C - carlgonz@uchile.cl
 * @date 2020
 * @copyright GNU GPL v3
 *
 * This task implements a listener that send commands scheduled in the flight
 * plan
 */

#ifndef T_SIMFLIGHTPLAN_H
#define T_SIMFLIGHTPLAN_H

#include <suchai/log_utils.h>
#include <stdlib.h>
#include <time.h>

#include "suchai/config.h"

#include "suchai/osQueue.h"
#include "suchai/osDelay.h"

#include "suchai/repoCommand.h"
#include "suchai/repoData.h"
#include "app/system/repoDataSim.h"

void taskSimFlightPlan(void *param);

#endif //T_SIMFLIGHTPLAN_H
