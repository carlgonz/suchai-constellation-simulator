/**
 * @file  taskHousekeeping.h
 * @author Tomas Opazo T - tomas.opazo.t@gmail.com
 * @author Carlos Gonzalez C - carlgonz@uchile.cl
 * @date 2020
 * @copyright GNU GPL v3
 *
 * This task implements a listener, that sends commands at periodical times.
 */

#ifndef T_TIMER_H
#define T_TIMER_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <time.h>

#include "suchai/config.h"
#include "suchai/globals.h"

#include "suchai/osQueue.h"
#include "suchai/osDelay.h"
#include "suchai/osThread.h"

#include "suchai/repoCommand.h"

typedef struct sim_args_s{
    time_t sim_start_s;
    time_t sim_end_s;
    int sim_step_ms;
    int sim_sleep_ms;
    int sim_id;
    int sim_secondaries;
}sim_args_t;

/**
 * Task that controls simulated time and write ticks to secondary apps
 * @param param
 */
void taskTimerPrimary(void *param);

/**
 * Task that reads simulated ticks from primary app and setup OS clock
 * @param param
 */
void taskTimerSecondary(void *param);

#endif //T_TIMER_H




