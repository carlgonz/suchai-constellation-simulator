cmake -B build -G Ninja -DAPP=simulator -DSCH_COMM_ENABLE=0 -DSCH_OS=SIM -DSCH_ST_MODE=RAM -DSCH_CON_ENABLED=1 -DSCH_HOOK_COMM=1 -DSCH_FP_ENABLED=0 -DSCH_CSP_CONN_TIMEOUT=10 -DSCH_CSP_READ_TIMEOUT=1
cmake --build build
exit $?
